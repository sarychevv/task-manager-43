package ru.t1.sarychevv.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.task.*;
import ru.t1.sarychevv.tm.dto.response.task.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    String NAME = "TaskEndpoint";

    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST)
                                                @NotNull TaskBindToProjectRequest request);

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(@WebParam(name = REQUEST, partName = REQUEST)
                                                        @NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(@WebParam(name = REQUEST, partName = REQUEST)
                                                      @NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    @WebMethod
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                            @NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    @WebMethod
    TaskClearResponse clearTask(@WebParam(name = REQUEST, partName = REQUEST)
                                @NotNull TaskClearRequest request);

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(@WebParam(name = REQUEST, partName = REQUEST)
                                  @NotNull TaskCreateRequest request);

    @NotNull
    @WebMethod
    TaskGetByIdResponse getTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                    @NotNull TaskGetByIdRequest request);

    @NotNull
    @WebMethod
    TaskGetByIndexResponse getTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                          @NotNull TaskGetByIndexRequest request);

    @NotNull
    @WebMethod
    TaskListByProjectIdResponse getTaskByProjectId(@WebParam(name = REQUEST, partName = REQUEST)
                                                   @NotNull TaskListByProjectIdRequest request);

    @NotNull
    @WebMethod
    TaskListResponse listTask(@WebParam(name = REQUEST, partName = REQUEST)
                              @NotNull TaskListRequest request);

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                          @NotNull TaskRemoveByIdRequest request);

    @NotNull
    @WebMethod
    TaskRemoveByIndexResponse removeTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                @NotNull TaskRemoveByIndexRequest request);

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                          @NotNull TaskUpdateByIdRequest request);

    @NotNull
    @WebMethod
    TaskUpdateByIndexResponse updateTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                @NotNull TaskUpdateByIndexRequest request);

}
