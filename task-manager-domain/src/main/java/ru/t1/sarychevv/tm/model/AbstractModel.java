package ru.t1.sarychevv.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractModel implements Serializable {

    @Id
    @NotNull
    @Column(nullable = false)
    protected String id = UUID.randomUUID().toString();

    @NotNull
    @Column(nullable = false)
    protected Date created = new Date();

}
