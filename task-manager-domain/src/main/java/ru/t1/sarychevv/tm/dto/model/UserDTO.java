package ru.t1.sarychevv.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_user")
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(nullable = false)
    private String login;

    @Nullable
    @Column
    private String password;

    @NotNull
    @Column
    private String email = "";

    @NotNull
    @Column(name = "first_name")
    private String firstName = "";

    @NotNull
    @Column(name = "last_name")
    private String lastName = "";

    @NotNull
    @Column(name = "middle_name")
    private String middleName = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column
    private Boolean locked = false;

}

